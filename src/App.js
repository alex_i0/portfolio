import React, { Component } from 'react';
import './App.css';
import Sidebar from './components/Sidebar/Sidebar';
import ContentContainer from'./components/ContentConatiner/ContentContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Sidebar />
          <ContentContainer />
      </div>
    );
  }
}

export default App;
