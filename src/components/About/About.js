import React from 'react';
import './About.css';
import profile from '../../assets/personal-photo.jpg';

const about = (props) => {
    return(
        <div className="about-container">
            <div className="about-title">

                <h2>About me</h2>
            <p>As you can known from the header my name is Aleksander, I was born in Leszno, middle-sized city between Poznan and Wroclaw. I’m third class student of Technical High School of Computer Science <i class="fas fa-graduation-cap"></i>.</p>
            <p>I’m interested in programming for 2 years. I have spend most of this time mainly learning essential, then complex parts of JavaScript <i class="fab fa-js-square"></i>. Then came Node.js, ES6 and framework React. With React came … hate, then understanding and finally… love.</p>
            <p>In the near time I’m planning to find the internship for one month time period since November 19 to December 14. If you work in company <i class="fas fa-building"></i> that could hire me as an intern, where I could participiate on complex projects, I would be grateful if you let me know.</p>
            <p>In my free time i like to travel across Europe (Rome you are next one <i class="fas fa-globe-americas"></i>), train some heavy-lifting at a local gym or watch another Netflix series. After hard day or programming session I enjoy to play computer games <i class="fas fa-gamepad"></i> which helps me clear my mind out. </p>
            </div>

            <div className="about-table">
                <table>
                    <tr><img src={profile} alt=""/></tr>
                    <tr><td>Aleksander Karp, 18</td></tr>
                    <tr><td>Leszno Poland</td></tr>
                    <tr><td>Polish, English</td></tr>
                    <tr><td>Technical High School</td></tr>
                </table>
            </div>
            
        </div>
    );
}

export default about;