import React from 'react';
import './Contact.css';

const contact = (props) => {
    return (
        <div className="contact-container">

            <div className="contact-text-container">

            
            <h2>Get in touch</h2>
            <p>In the near time I’m planning to find the internship for one month time period since November 19 to December 14. If you work in company <i class="fas fa-building"></i> that could hire me as an intern, where I could participiate on complex projects, I would be grateful if you let me know.</p>
            <p>If you have any questions or suggestions, which can help me finding internship as programmer, please contact me. I would appreciate any feedback.
            </p>

            </div>

            <ul className="contact-icons">
                <div><i className="fas fa-phone-square fa-2x"></i>+48 783 228 444</div>  
                <div><i className="fas fa-envelope-square fa-2x"></i>aleksander.karp2000@gmail.com</div>  
                <div><i className="fab fa-linkedin fa-2x"></i> <a href="https://www.linkedin.com/in/aleksander-karp-b2279b155/" target="_blank">LinkedIn</a> </div>  
            </ul>
        </div>
    );
}

export default contact;
