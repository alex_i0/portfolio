import React from 'react';
import './ContentContainer.css';
import {
    BrowserRouter as Router,
    Route,
    Link
  } from 'react-router-dom';

  import Home from '../Home/Home';
  import About from '../About/About';
  import Skills from '../Skills/Skills';
  import Contact from '../Contact/Contact';
  import Projects from '../Projects/Projects'

const contentContainer = (props) => {
    return(
        <Router>
        <div className="content-container">

                    
                    <div className="top-headers">
                        <h1>Aleksander Karp</h1>
                        <h2>JavaScript Developer</h2>
                    </div>
                    

                    <ul className="panel">
                    
                        <Link to="/about"><li>About me</li></Link>
                    
                    
                        <Link to="/skills"><li>Skills</li></Link>
                    
                    
                        <Link to="/projects"><li>Projects</li></Link>
    
                        <Link to="/contact"><li>Contact</li></Link>
                    
                </ul>


            <section>
                                
                    <div className="content">
                        <Route exact path="/" component={Home} />
                        <Route path="/about" component={About} />
                        <Route path="/contact" component={Contact} />
                        <Route path="/skills" component={Skills} />
                        <Route path="/projects" component={Projects} />
                    </div>
                

                
            </section> 
        </div>
        </Router>
    );
}

export default contentContainer;