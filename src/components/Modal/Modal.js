import React from 'react';
import './Modal.css';
import apollo from "../../assets/apollo-thumbnail.jpg";
import yelpcamp from "../../assets/yelpcamp-thumbnail.jpg";
import burger from "../../assets/burger-thumbnail.jpg";
import terminal from "../../assets/terminal-thumbnail.jpg";



const name = (props) => {

    let data;
    let imageSrc;

    switch (props.project) {
        case "apollo":
            data = props.data.apollo;
            imageSrc = apollo;
            break;
        case "terminal":
            data = props.data.terminal;
            imageSrc = terminal;
            break;
        case "burgerBuilder":
            data = props.data.burgerBuilder;
            imageSrc = burger;
            break;
        case "yelpCamp":
            data = props.data.yelpCamp;
            imageSrc = yelpcamp;
            break;
        default:
            data = props.data.apollo;
            break;
    }



    return (
        <div className="modal" style={{
            transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: props.show ? '1' : '0'
        }}>

            <div className="modal-title">
                <h2>{data.title}</h2>
                <hr />
            </div>
            
            <p>{data.description}</p>
            <img src={imageSrc} alt=""/>

            <div className="modal-button-container">
                <a className="modal-button" href={data.git}>GitLab</a>
                <a className="modal-button" href={data.demo}>Live Demo</a>
            </div>
            
            <div className="modal-tools">
                <p>{data.tools}</p>
            </div>
            

        </div>
    );
}

export default name;