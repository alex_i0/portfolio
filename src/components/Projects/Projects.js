import React, { Component } from 'react';
import './Projects.css';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import apollo from "../../assets/apollo-thumbnail.jpg";
import yelpcamp from "../../assets/yelpcamp-thumbnail.jpg";
import burger from "../../assets/burger-thumbnail.jpg";
import terminal from "../../assets/terminal-thumbnail.jpg";
import Modal from '../Modal/Modal';
import Backdrop from '../Backdrop/Backdrop';



const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    expandedProject: ""
};

const projectDetails = {
    apollo: {
        title: "Apollo Todo App",
        description: "/STILL IN PROGRESS/ Apollo is the simplest todo app as it only can be. Main goal is to create fast, intuitive and simple app for making daily notes.",
        tools: "javascript, html, css, react, node, express, mongoDB, webpack",
        git: "https://gitlab.com/alex_i0/apollo-todo-app",
        demo: "https://rocky-ridge-28609.herokuapp.com/"
    },
    terminal: {
        title: "Roboco Terminal Hacking",
        description: "/STILL IN PROGRESS/ Terminal hacking game based on minigame from Fallout 4. The main goal is to guess the right password.",
        tools: "javascript, html, css, react, webpack",
        git: "https://gitlab.com/alex_i0/terminal-password-game",
        demo: "https://rocky-ridge-28609.herokuapp.com/"
    },
    burgerBuilder: {
        title: "Burger Builder App",
        description: "Build on framework React app that allows you to customizated your own burger and make an order.",
        tools: "javascript, html, css, react, webpack",
        git: "https://gitlab.com/alex_i0/burger-builder-app",
        demo: "https://rocky-ridge-28609.herokuapp.com/"
    },
    yelpCamp: {
        title: "YelpCamp App",
        description: "App that allows you too book place for camping. There are features like making account, logging, creating camplace or writing comments",
        tools: "javascript, html, css, express, mongoDB, node",
        git: "https://gitlab.com/alex_i0/yelp_camp",
        demo: "https://rocky-ridge-28609.herokuapp.com/"
    }
}


class Projects extends Component {

    state={
        show: false
    }

    modalShowHandler = () => {
        this.setState(prevState => ({
            show: !prevState.show
        }))
    }

    render() {
        return (

            <React.Fragment>
                <Backdrop show={this.state.show} toggle={()=> this.modalShowHandler()} />
                <Modal show={this.state.show} data={projectDetails} project={this.state.expandedProject}/>
                <div className="projects-container">
                    <h2>Projects</h2>


                    <Slider {...settings}>
                        <div className="slider slider-one">
                            <img src={apollo} alt="" />
                            <h3>Apollo Todo</h3>
                            <button className="project-button" style={{"color": "#eb4d4b"}} onClick={() => {this.modalShowHandler(); this.setState({
                                expandedProject: "apollo"    
                            })}}>Expand</button>
                        </div>


                        <div className="slider slider-two">
                            <img src={yelpcamp} alt="" />
                            <h3>YelpCamp</h3>
                            <button className="project-button" style={{"color": "#5cb85c"}} onClick={() => {this.modalShowHandler(); this.setState({
                                expandedProject: "yelpCamp"    
                            })}}>Expand</button>
                        </div>


                        <div className="slider slider-three">
                            <img src={burger} alt="" />
                            <h3>BurgerBuilder</h3>
                            <button className="project-button" style={{"color": "#CF8F2E"}} onClick={() => {this.modalShowHandler(); this.setState({
                                expandedProject: "burgerBuilder"    
                            })}}>Expand</button>
                        </div>


                        <div className="slider slider-four">
                            <img src={terminal} alt="" />
                            <h3>Terminal hacker</h3>
                            <button className="project-button" style={{"color": "#303030"}} onClick={() => {this.modalShowHandler(); this.setState({
                                expandedProject: "terminal"    
                            })}}>Expand</button>
                        </div>


                    </Slider>
                </div>
            </React.Fragment>
        );
    }
}

export default Projects;