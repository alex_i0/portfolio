import React from 'react';
import './Sidebar.css';

const sidebar= (props) => {
    return(
        <aside>
            <div className="photo-square">
                <div className="personal-photo"></div>
            </div>
            

            <ul>
                <li>
                    <a href="https://gitlab.com/alex_i0" target="_blank"><i className="fab fa-gitlab fa-3x"></i></a>
                </li>
                <li>
                    <a href="https://www.linkedin.com/in/aleksander-karp-b2279b155/" target="_blank"><i className="fab fa-linkedin-in fa-3x"></i></a>
                </li>
                <li>
                    <a href="mailto: aleksander.karp2000@gmail.com" target="_blank"><i className="fas fa-envelope fa-3x"></i></a>
                </li>
                <li>
                    <a href="https://www.facebook.com/alek.karp" target="_blank"><i className="fab fa-facebook-f fa-3x"></i></a>
                </li>
            </ul>

        </aside>
    );
}

export default sidebar;