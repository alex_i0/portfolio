import React from 'react';
import './Skills.css';

const skills = (props) => {

    return (
        <div className="skill-container">

            <div className="skills-title">
            </div>
            

            <div className="tech-skills">
                <h3>Tech skills</h3>
                <div className="tech-skills-flexbox">
                    <div className="skill-square">
                        <i className="fab fa-html5 fa-5x"></i>
                        <h3>HTML</h3>
                    </div>

                    <div className="skill-square">
                        <i className="fab fa-css3-alt fa-5x"></i>
                        <h3>CSS</h3>
                    </div>

                    <div className="skill-square">
                        <i className="fab fa-js-square fa-5x"></i>
                        <h3>JavaScript</h3>
                    </div>

                    <div className="skill-square">
                        <i className="fab fa-node fa-5x"></i>
                        <h3>Node.js</h3>
                    </div>

                    <div className="skill-square">
                        <i className="fab fa-react fa-5x"></i>
                        <h3>React</h3>
                    </div>

                    <div className="skill-square">
                        <i className="fab fa-envira fa-5x"></i>
                        <h3>MongoDB</h3>
                    </div>

                    <div className="skill-square">
                        <i className="fas fa-server fa-5x"></i>
                        <h3>Express</h3>
                    </div>

                    <div className="skill-square">
                        <i className="far fa-window-restore fa-5x"></i>
                        <h3>REST</h3>
                    </div>

                </div>

            </div>

            <div className="other-skills">
                <h3>Other skills and Certifications</h3>
                <ul>
                    <li>English - fluent</li>
                    <li>Polish - native</li>
                    <li>Driving licence (category B)</li>
                    <li>Certifiaction E12 - Assembly and operations of personal computers and peripherals</li>
                    <li>Certifiaction ECDL PTI Standard</li>
                    <li>Udemy Certifications</li>
                </ul>
            </div>

            <div className="tool-skills">
                <h3>Tools I use</h3>
                <div className="tool-skills-flexbox">
                    <div className="skill-square">
                        <i className="fas fa-terminal fa-5x"></i>
                        <h3>CLI</h3>
                    </div>

                    <div className="skill-square">
                        <i className="fab fa-git-square fa-5x"></i>
                        <h3>GIT</h3>
                    </div>

                    <div className="skill-square">
                        <i className="fab fa-microsoft fa-5x"></i>
                        <h3>MS Office</h3>
                    </div>

                    <div className="skill-square">
                        <i class="fab fa-npm fa-5x"></i>
                        <h3>npm</h3>
                    </div>

                </div>

            </div>

        </div>
    );
}

export default skills;